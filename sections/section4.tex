\begin{center}
\begin{table*}[t]
\caption{Analysis of the W-Diameter of real life data sets.}
\label{table:table1}
\centering
\begin{tabular}{|l||ccc|cc|cc|cc|}
\hline
					&				&					&				& \multicolumn{2}{c|}{Double BFS}				& \multicolumn{2}{c|}{Dynamic}	  & \multicolumn{2}{c|}{Multiple BFS}	\\
Dataset             & Vertices      & Diameter          & Iterations    & Time (s)           	& W-Diam                & Time (s)         & W-Diam        & Time (s)            & W-Diam      \\
\hline
vanc                & 378           & 311               & 2             & 0.000413              & 2                     & 0.000497        & 2             & 0.022195            & 2           \\
vancouverSWSW       & 1225          & 845               & 3             & 0.000918              & 2                     & 0.002317        & 2             & 0.231720            & 2           \\
vancouverSWNE       & 1250          & 423               & 4             & 0.000937              & 5                     & 0.001858        & 5             & 0.250005            & 5           \\
vancouverSWNW       & 1250          & 712               & 3             & 0.000798              & 3                     & 0.002454        & 3             & 0.252271            & 3           \\
vancouverSWSE       & 1275          & 759               & 3             & 0.001009              & 3                     & 0.00204         & 3             & 0.252729            & 3           \\
\hline
vancouverNE         & 4851          & 1338              & 5             & 0.003637              & 4                     & 0.005493        & 5             & 3.899871            & 5           \\
\hline
vancouverNW         & 4900          & 1456              & 5             & 0.002893              & 5                     & 0.005612        & 5             & 3.967709            & 5           \\
vancouverSE         & 4950          & 1306              & 5             & 0.002958              & 6                     & 0.005863        & 6             & 4.002013            & 6           \\
vancouverSW         & 5000          & 1977              & 4             & 0.002795              & 4                     & 0.005658        & 4             & 4.049257            & 4           \\
icefield            & 57600         & 12280             & 6             & 0.036430              & 7                     & 0.066978        & 7             & 717.305936          & 7           \\
pukaskwa            & 881600        & 374866            & 94            & 0.520608              & 180                   & 0.962897        & 180           & N/A                 & N/A          \\
gtopo30w020n40      & 28800000      & 15766966          & 8             & 19.103233             & 8                     & 34.427916       & 8             & N/A                 & N/A          \\
\hline
\end{tabular}
\end{table*}
\end{center}


\section{Empirical Study}
\label{sec:results}
In this section we supplement the theoretical investigation of the W-structures with an empirical study. 
We verify the correctness and running time of the w-diameter algorithms and study the W-structures present in the contour trees of real life data sets.

\subsection{Algorithm Implementations}

To conduct the empirical study we implemented all three w-diameter algorithms.
%For NxBFS and 2xBFS we modified the standard Breadth First Search to compute w-distances from a given vertex to all other vertices in the tree and then followed the algorithm description.
We avoided a direct implementation of the recursive formula of the Dynamic algorithm due to excessive overhead cause by recursive calls. 
Instead we traversed the tree once with a standard Breadth First Search to identify the children and parents of all vertices. We then put them in a list and starting from the leaves we processed all vertices in the tree making sure their children are processed beforehand.
The algorithms were implemented in C++ and their source code is in the supplementary materials.

\subsection {Data set Overview and Testing Methodology}

The real life data sets we used are taken from the GTOPO30 \cite{gtopo} digital elevation model of the world. 
GTOPO30 is a two dimensional data grid containing the elevation of points on Earth with a resolution of approximately one kilometer. 
Due to the size of the data set we have taken several smaller subsets of GTOPO30 based on their their topographic complexity. 
%They are a mix of mountainous and lowland regions in Canada. 
You can see the names of the data sets in the first column of Table \ref{table:table1}.
The data set {\em vanc} and all other {\em vancouver} data sets are taken from the North Shore Mountains that overlook Vancouver in British Columbia, Canada. 
The data set {\em pukaskwa} is taken from the Pukaskwa National Park located south of the town of Marathon, Ontario, Canada. 
The data set {\em icefields} is taken from Icefields Parkway in the heart of the Canadian Rocky Mountain; it straddles the continental divide. 
Finally {\em gtopo30w020n40} is a rectangular tile whose upper left corner is at 20 degrees west longitude and 40 degrees north latitude. It covers 40 degrees of longitude and 50 degrees of latitude.

All tests were run on a Lenovo E550 Laptop with Intel(R) Core(TM) i5-5200U CPU at 2.20GHz and 8GB DDR3 RAM at 1600 MHz.
Each one of the data sets was run five times to get the average of the running time of the w-diameter programs.

\subsection {Results}

The results of the empirical tests are shown in Table \ref{table:table1}. 
The first column of the table shows the name of the data sets, the second column gives the number of vertices in the mesh and the third is the diameter of the contour tree (not w-diameter).
The fourth column shows the number of iterations that the data-parallel algorithm takes to produce the contour tree in the merge phase.
The following six columns give the running times and w-diameter output of the Double BFS, Dynamic and Multiple BFS algorithms.
Note that the Multiple BFS algorithm could not be run on the last two data sets because because of their size.

The most noteworthy result is that W-structures appear in the contour trees of real world data and hinder parallel performance.
This is evident from the w-diameter and number of iterations on the pukaskwa data set.
They are bigger than the other data sets and clearly demonstrate that logarithmic collapse in not taking place in the merge phase.
If it were, we would expect $\left \lceil{log_2(881600)}\right \rceil = 20$ iterations. 
The fact that the w-diameter of the pukaskwa contour tree is 180 forks long means that it requires at least 90 iterations to collapse because one iterations prunes one fork from each side of the w-diameter.
This is consistent with the number of iterations it actually takes for the merge phase which is 94. 

Furthermore by comparing the w-diameter and the actual diameter of the contour trees we can see that the w-diameter is a much better bound on the steps of the data parallel algorithm.
In the most extreme example, that of gtopo30w020n40, the w-diameter of the augmented contour tree is 1,970,870 times smaller than its diameter.
While not as good as logarithmic collapse we have shown that the w-diameter is a better bound in practice.

Finally the running time and correctness of the w-diameter algorithms is consistent with our theory. 
The worst case running time for Dynamic is quadratic, but as predicted it is not exhibited and in practice it is around twice as slow as the Double BFS algorithm.
The Dynamic and Multiple BFS algorithms produce the same results, while the Double BFS algorithm produces a suboptimal w-path in the highlighted vancouverNE data set.

%Our goal is to examine the w-structures present in the contour trees of real life data sets. 
%We will use our w-diameter algorithms to find the largest w-structure in these contour trees and demonstrate that it not only poses a theoretical difficulty, but also hinders practical performance.  
%To measure the effect the w-diameter has on the computation we will examine it together with the iterations needed to fully collapse the contour tree. 
%The close relation between the two will give us reason to believe the w-diameter is indeed prevent the expected logarithmic collapse in the merge phase of the data-parallel algorithm. 
%This will allow us to present a better upper bound on the steps of the data-parallel contour tree algorihtm.

%The first inference we can make is about the pukaskwa data set. 
%Both the w-diameter and number of iterations are drastically bigger than all of the other data sets. 
%If logarithmic collapse was taking place in the merge phase of the construction of the contour tree of pukaskwa then we would expect that to take $\left \lceil{log_2(881600)}\right \rceil = 20$ iterations. 
%Instead it takes $94$ iterations. 
%This is consistent with the w-diameter of the data set. 
%Consier that the algorithm can process exactly two branches on opposite sides of the w-diameter in a single iterations. 
%It follows that at least $180/2 = 90$ iterations are needed for the full w-diameter to be collapsed.

%Secondly, we can confirm that the w-diameter in almost all of the data sets (except for vancouverSWSW) is bigger than or equal to the number of iterations. 
%This leads us to believe that the two may be correlated. 
%In the case of pukaskwa we have already given an interpretation of this correlation. 
%By that reasoning we would expect that in other data sets the w-diameter to be twice as much as the number of iterations. 
%This is not the case and this may very well be due to how different w-structures interact with one another in the merge phase. 
%This is something we have not investigated as we only record the size of the largest w-structure.

% Another interesting thing to note is that pukaskwa is a subset of the gtopo30w020n40 dataset and yet it's w-diameter is far smaller. This proves that the contour trees of subsets of data need not have a smaller w-diamete. Strangely enough as a consequence of this subsets of data may not be faster to compute. This has

%Finally we turn our attention to the diameter of the augmented and unaugmented contour trees. 
%The parallel contour tree algorithm currently uses those as an upper bound on the time complexity of the merge phase \cite{parallel-peak-pruning}. 
%We have already shown theoretically that the w-diameter of a contour tree is necessarily smaller than its diameter. 
%This test demonstrates how big the difference between the two can be in practice. 
%In the most extreme example, that of gtopo30w020n40, the w-diameter of the augmented contour tree is 1,970,870 times smaller than its diameter. 
%If the w-diameter of that data sets were equal to the actual diameter, than we would expect the merge phase to take a far larger number of iterations and severely limit the available parallelism in it.
