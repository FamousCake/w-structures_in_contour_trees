\section{W-structures in Contour Trees}
\label{sec:wstructures}

We will now formalize our approach to understanding the W-structures. We adopt the following notation: in a contour tree $T$ the set of vertices is $V$ and the set of edges is $E$. We refer to paths in the contour tree by their first and last vertex because there is a unique path between any two vertices.

%Let us first state some of the basic definitions we will make use of. A path in a graph is a sequence of distinct and adjacent vertices. When dealing with paths in trees we will refer to them by their first and last vertex, because there is a unique path between any two vertices in a tree. A subpath $P'$ of a path $P$ is a path whose vertices are also vertices of $P$.

\subsection{Spatial Characterization}

An important property of paths in contour trees is their monotone path decomposition. This is a sequence of monotone subpaths that share exactly one vertex and have alternating direction (Figure \ref{fig:monotone-decomposition}). We can use the number of subpaths in the monotone path decomposition to characterize them. To simplify this characterization note that the number of subpaths in the monotone path decomposition is one more than the number of vertices where an ascending subpath ends and a descending subpath begins (or vice versa). We will call there vertices forks.


\begin{figure}%
    \centering
    \subfloat[Extracted W-structure.]{{\includegraphics[scale=0.04]{./images/w-path2.pdf}}}%
    \qquad
    \subfloat[Monotone path decomposition.]{{\includegraphics[scale=0.04]{./images/w-path2-decomposed.pdf}}}%
    \caption{A W-structure and its monotone path decomposition (forks in solid black).}
    \label{fig:monotone-decomposition}%
\end{figure}

We define the number of forks in a path to be the path's w-length.
This is analogous to how the number of edges in a path is its length.
To avoid ambiguity we will also use the term w-path to emphasize that we are referring to a path's w-length.
Note that if two paths share a vertex it may be a fork in one of them, but not the other.
For example the vertex $5$ from the contour tree in \figref{mesh-join-split-contour} is a fork in the path from $6$ to $9$, but not in the path from $1$ to $8$.
This property is crucial in understanding how we develop algorithms for detecting W-structures.

In this terminology the largest W-structure in a contour tree is a path between two leaves with maximum w-length (or longest w-path).
We will call this the {\em w-diameter} of the contour tree.
This again is analogous to the longest path in a tree called the diameter of the tree.
As there are efficient algorithms for computing the diameter of a tree a natural question to ask is whether we can adapt these algorithms to compute the w-diameter of a contour tree.

\begin{figure*}[]%
    \centering
    \subfloat[Stage 1.]{{\includegraphics[scale=0.10]{./images/proofs/stages/stage1.pdf}}}%
    \qquad
    \subfloat[Stage 2.]{{\includegraphics[scale=0.10]{./images/proofs/stages/stage2.pdf}}}%
    \qquad
    \subfloat[Actual w-diameter.]{{\includegraphics[scale=0.10]{./images/proofs/stages/stage3.pdf}}}%
    \caption{Execution of the Double BFS algorithm on an example contour tree and the actual w-diameter of the contour tree. Black edges indicate a path of maximum w-length. Numerical labels next to vertices indicate their w-distance from the start vertex. In stage 1 we start from an arbitrary vertex $s$ and find the most w-distant vertex from it $u$. In stage 2 we find the most w-distant vertex from $u$ and call it $v$. The w-length of the path from $u$ to $v$ is however suboptimal as demonstrated in the last figure. It our initial root was $a$ then the algorithm would have obtained the w-diameter.}
    \label{fig:double_bfs_example}%
\end{figure*}


\subsection{W-Diameter Algorithms}
\label{sec:wdiamalgo}

We discuss three known tree diameter algorithms \cite{intro-to-algo}. The first one is the brute force approach of computing the length of all paths in the tree and outputting the maximum one. This algorithm has quadratic running time.  The second one is based on the proposition that the most distant leaf from any vertex in a tree is the endpoint of the diameter of the tree \cite{computer-recreations}. This algorithm can be computed in linear time. The third algorithm is based on the dynamic programming paradigm. We start by selecting a root for the tree. We then observe that the longest path in the tree is either entirely contained in the subtree of one of the children of the root or it starts from a leaf in one of the subtrees, passes through the root and ends in a leaf of another subtree. This algorithm can be computed in linear time as well. We now present our modifications of these algorithms.

\subsection{Algorithm 1 - Multi BFS}

The brute force approach to finding the w-diameter of the contour tree compares the w-lengths of all paths in the contour tree. 
To implement it we modify Breadth First Search (BFS) to traverse the tree and compute w-length instead of length.
We then run this modified BFS from every vertex in the tree and output the maximum value found.
Therefore it has quadratic running time.
We will accordingly refer to this algorithm as Multiple BFS in the future.

\subsection{Algorithm 2 - Double BFS}

The algorithm works in the same manner as the original tree diameter algorithm except we measure w-distance instead of distance.
Consider a contour tree $T$ and an arbitrary start vertex $s$.
First we find the most w-distant vertex from $s$ and call it $u$.
Then we find the most w-distant vertex from $u$ and call it $v$.
The difference however with the original algorithm is that we are not guaranteed to land on an endpoint of a w-diameter after the first search.
However we claim that we are guaranteed to land on an endpoint of a near optimal path.
The w-length of this path is at least that of the w-diameter minus two.

We illustrate this with an example in Figure \ref{fig:double_bfs_example}. 
In this example our algorithm does not produce the w-diameter of the contour tree which is the path from $a$ to $b$.
The mismatch between the output of the algorithm and the actual w-diameter is due to vertices which are forks in one path, but not in others. 
First consider the black vertex on the path from $s$ to $u$ in Figure \ref{fig:double_bfs_example} a); it is a fork on the path from $s$ to $u$, but not on the path from $u$ to $v = b$ as we see in Figure \ref{fig:double_bfs_example} b). 
Secondly, consider the midpoint of the path from $a$ to $b$; it is a fork on the path from $a$ to $b$, but not on the path from $u$ to $b$ or $a$. 

Our argument for the general case is based on this example. 
There are at most two such turning points which may or may not be forks during the course of the algorithm.
This causes the w-length of the path we find to vary by at most two from the w-diameter of the contour tree.

To implement Double BFS we pick a starting vertex and then run the modified Breadth First Search twice. 
The first BFS from the vertex to find the farthest leaf from it and second BFS from that leaf.
The algorithm consists of two consecutive Breadth First searches and therefore its running time is $O(|V|)$.

\subsection{Algorithm 3 - Dynamic}

The third algorithm works by progressively combining paths from subtrees of the contour tree together to obtain the longest w-path.
For a contour tree $T$ we pick an arbitrary start vertex $s$ to be the root of $T_s$.
Observe that the w-diameter of $T$ either passes through $s$ or it does not.
If it does pass through $s$ then it must also pass also through two children of $s$ and be contained in their subtrees.
If it does not pass through $s$ then it must be entirely contained in the subtree of one of the children of $s$.
We can then extend this reasoning recursively to all the subtrees of $T_s$.

For every vertex $u$ in $T_s$ there is a subtree of $T_{s,u}$ whose root is $u$. 
We will find the w-diameters of all such subtrees of $T_{s, u}$ and use them to compute the w-diameter of $T_s$.
We will now demonstrate how to compute the w-diameter of $T_{s, u}$ assuming that the optimal solutions for the subtrees whose roots are the children of $u$ have been found recursively.
Note that the base case for the recursion is at the leaves of the tree Figure \ref{fig:dp_example} a).




\begin{figure*}[]%
    \centering

    \subfloat[Stage 1]{{\includegraphics[scale=0.10]{./images/proofs/stages/dpstage1.pdf}}}%
    \qquad
    \subfloat[Stage 2]{{\includegraphics[scale=0.10]{./images/proofs/stages/dpstage2.pdf}}}%
    \qquad
    \subfloat[Stage 3]{{\includegraphics[scale=0.10]{./images/proofs/stages/dpstage3.pdf}}}%
    \qquad
    \subfloat[Stage 4]{{\includegraphics[scale=0.10]{./images/proofs/stages/dpstage4.pdf}}}%

    \caption{Execution of the Dynamic algorithm on an example contour tree. Black vertices are already processed, gray ones are not. The black edges are the w-diameters for all subtrees. The numerical label next to a vertex indicates that is has been processed in the current stage and the number is the w-diameter in its subtree. Each stage iteratively combines w-diameters of smaller subtrees to obtain the diameter of the whole tree. }

    \label{fig:dp_example}%
\end{figure*}



\textbf{Case 1} - the w-diameter of $T_{s, u}$ goes through $u$.
To handle this case we must find two maximum paths contained in two subtrees whose roots are children of $u$, say $a$ and $b$.  
As we have recursively found all such maximum paths we only have to determine which combination of two of them gives a maximum w-diameter.
When combining them {\em three} vertices can become forks. 
The first one is $u$ and the other two are $a$ and $b$ which were previously endpoints of the maximum paths in their subtrees.
To account for $u$ we simply have to compare its height to $a$ and $b$.
To account for $a$ and $b$ we must compare their height with $u$ and the previous vertex in the maximum w-path they are the endpoint of.

An example of where Case 1 holds is in Figure \ref{fig:dp_example} c).
Note that this requires us to not only look at all children of $u$, but also to all children of children of $u$. 
In addition to this it may be the case that $u$ is a leaf and has only one child say $a$. 
In this case $u$ must be the endpoint of the w-diameter.
In this case we find all maximum paths that end at $a$ and account for whether $a$ becomes a fork in them or not.
%What we must do is pick the maximum w-path that end 


\textbf{Case 2} - the w-diameter of $T_{s, u}$ does not pass through $u$.
In this case the w-diameter has to be entirely contained in one of the subtrees whose root is a child of $u$.
An example of where Case 1 holds is in Figure \ref{fig:dp_example} d).

Let us now derive the time complexity of the algorithm. 
Firstly it takes $O(|V|)$ time to traverse the tree and root it via either BFS or DFS. 
Secondly, we iterate over the children of all children of all vertices. Since the algorithm operates on trees, every vertex has a unique grandparent. Therefore every vertex will be visited exactly once and contribute $O(|V|)$. 
Finally we pick all pairs of children of a vertex to find the maximum w-path that goes through the vertex. Computing this for all vertices in the graph yields $O(\sum_{u \in V}{d(u)^2})$ where $d(u)$ is the degree of a vertex. To see how we can evaluate this consider that in a tree $d(u) + d(v) \le |V|$ for any two vertices connected by an edge (otherwise we would have a cycle). If we sum over all edges we obtain that $ \sum_{uv \in E(T)}{d(u) + d(v)} \le |V|^2$. In the summation on the left hand side every term $d(u)$ is present $d(u)$ times and therefore $ \sum_{uv \in E(T)}{d(u) + d(v)} = \sum_{u \in V(T)}{d(u)^2} \le |V|^2$. Therefore the overall complexity of the algorithm is $O(|V|^2)$.

We have shown that the algorithmic complexity of the Dynamic algorithm is no better than that of the Multiple BFS algorithm.
However in practice this will be highly dependent on the average degree of the vertices of the contour tree and will even have linear complexity for trees of bounded width.
