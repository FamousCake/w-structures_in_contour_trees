\section{W-structures Simplification}
\label{sec:phbd}

Contour tree simplification via branch decomposition was introduced by Pascucci et.~al \cite{ct-branch-decomp} in 2004.
They state that there is a relationship between the branch decomposition of the contour tree and persistent homology, but do not derive it formally within the framework of persistent homology.
While the relationship is obvious we show that the two are not equivalent with a counter-example based on the W-structures.
In this counter-example the pairs of critical points produced by branch decomposition differ from the pairs of critical points produced by persistent homology.

%In 2004 Pascucci et. al published the original paper that introduced contour tree simplification via branch decomposition \cite{ct-branch-decomp}.
%In that paper it is clearly stated that there is a similarity between branch decomposition and persistent homology \cite{persistence-original}.
%The authors do not describe how the persistence of branches can be derived formally within the framework of persistent homology.
%However the relation between the two is not fully explored in the paper nor in subsequent publications.
%To rectify this we answer the following question: are the pairs of critical points produced by branch decomposition the same as the pairs produced by the persistent homology?


\begin{figure*}[]%
    \includegraphics[width=.86\textwidth]{./images/filtration/all-in-one-filtration.pdf}  

    \caption{ Ascending c) and Descending d) filtration.  Direction of travel of the two filtrations b) and e).  Branch decomposition of the split a) and the join tree f) with additional vertices corresponding to connected components.  }%
    \label{fig:filtration}%
\end{figure*}

\subsection{Persistent Homology Overview}

Persistent homology is a general tool for topological simplification \cite{persistence-original} that emerged in the early 2000s.
The building blocks of persistent homology are sequences of nested simplicial complexes called filtrations. 
In a filtration we start from the empty set and iteratively add simplices to obtain the full complex.
Our working example will be the ascending and descending filtrations on Figure \ref{fig:filtration} of the simplicial mesh from Figure \ref{fig:mesh-join-split-contour}.

The ascending filtration of $M$ is made of up complexes $M_i$ which contain all vertices whose value is lower than or equal to $i$ and all other simplices between them. 
The descending filtration of $M$ is made of up complexes $M^i$ which contain all vertices whose value is bigger than or equal to $i$ and all other simplices between them. 
The complexes of the ascending filtration are the sub-level sets of the mesh and their order is given by an ascending isovalued sweep.
Analogously the complexes in the descending filtration are the super-level sets of the mesh ordered by a descending isovalued sweep.
In short this is exactly the same as the sweep and merge algorithm we discussed in \secref{ctalgorithms}.

The homology groups describe the n-dimensional connectivity of a single simplicial complex.
Since we use contour trees to study connectivity we will only need to consider the 0th homology group.
For each step of the filtration the 0th homology group has exactly one 0th homology class for each connected component and vice versa.
The insight of persistent homology is that we can calculate the 0th homology groups of each complex in the filtration and track their evolution throughout the filtration.
When a connected component appears in the progression of the filtration we say that a 0th homology class is {\em born}.
When two connected components merge together the 0th homology class that corresponds to the younger component {\em dies} and the 0th homology class that corresponds to the older one {\em persists}. 

The output of persistent homology is the so called persistence pairs.
A persistence pair is a record of the birth and death of a 0th homology class.
The persistence pairs of a filtration give a basis for topological simplification (or cancellation).
The intuition behind this simplification is that classes that have persisted for a large number of steps are deemed significant while ephemeral classes are not.


\subsection{Comparison of Critical Point Pairs}

From the description of the 0th persistent homology it is clear that it is a different mathematical formalism that captures the same idea as the merge trees do.
What is left to see is whether the persistence pairs produced within the framework of persistent homology correspond to the cancellation pairs produced by branch decomposition.
Since monotone paths in the contour tree correspond to monotone paths in the simplicial mesh \cite{ct-big-paper} then branches obtained via branch decomposition correspond to valid topological cancellations in the simplicial mesh \cite{ct-branch-decomp}.
Therefore the pairs of critical points obtained via branch decomposition will correspond to pairs of critical points in the simplicial mesh and as such will be directly comparable to the persistence pairs produced by persistent homology.

We start by computing the persistence pairs of the ascending filtration given in Figure \ref{fig:filtration} c).
Observe that one connected component appears in the complex $M_1$, another one in the complex $M_2$ and they merge in $M_5$.
When the two merge the older one born in $M_1$ persists and the younger one born in $M_2$ dies.
We record the second event with the persistence pair $(2, 5)$.
For the rest of the filtration all complexes consist of a single connected component.
When the filtration is complete we are left with a 0th homology class which corresponds to the single connected component of the mesh.
This 0th homology class does not die in the filtration and so we give it an infinite persistence pair $(1, \infty)$.
We call such 0th homology classes {\em essential}.
%The homology classes which correspond to infinite persistence pairs are called essential.

In the descending filtration in Figure~\ref{fig:filtration}~d) we can see that three 0th homology classes are born in the complexes $M^9$, $M^8$ and $M^6$.
The one born in $M^8$ dies in $M^4$ when it merges with the one born in $M^9$ (because $M^9$ is older).
The one born in $M^6$ dies in $M^3$ when it merges with the one born in $M^9$.
The one born in $M^9$ does not die in the descending filtration because it represents the connected component of $M$ itself.
Therefore the persistence pairs are $(6, 3)$, $(8, 4)$ and $(9, \infty)$.

%Each filtration represent the construction of the final complex.
%For each connected component of this complex there will always be exactly one infinite persistence pair consisting of the oldest vertex of the comp and infitiy

%in short in represent it represent the lifespan of the entire component
%extended persistence captures this by pairing it with the last simplex (vertex) added
%in short the global minimum and global maximum of each connected component in the final complex are always paired
%don't have to be explicit about it
%since ct always has one component this amounts to saying the global min and the global max always pair.

The two infinite persistence pairs are an issue because they are a pair consisting of one critical point and not two.
This can be resolved with the introduction of extended persistence \cite{persistence-extended}.
The idea behind extended persistence is that we take the essential classes from the ascending and descending pass and pair those which correspond to the same connected component. 
More generally we know that in a simple domain extended persistence always pairs the global minimum with the global maximum.
Indeed the essential class that first appears in the ascending filtration is the global minimum and the essential class that first appears in the descending filtration is the global maximum.
Both correspond to the single connected component of the domain itself.
Therefore in our example the extended persistence pair for the ascending filtration is $(1, 9)$ and the extended persistence pair for the descending filtration is $(9, 1)$.

To conclude, the extended persistence pairs of the ascending filtration are $(2, 5)$ and $(1, 9)$, and the extended persistence pairs of the descending filtration are $(6, 3)$, $(8, 4)$ and $(9, 1)$.
These pairs are identical to the pairs produced by the branch decomposition of the split and join tree as shown in Figure \ref{fig:filtration}.
Observe that the ascending filtration cancels minima with split saddles and the descending filtration cancels maxima with join saddles.

It is therefore seductively plausible to construct the branch decomposition of the contour tree by taking cancellation pairs from the ascending and descending filtration.
However, the branch decomposition of the contour tree is not guaranteed to match either set of pairs.
To see why refer back to the branch decomposition of the contour tree in Figure \ref{fig:branch-decomp}.

Notice that the branch decomposition of the contour tree in effect zigzags its way along the W-structure.
While the first produced branch $(6, 3)$ is the same in the contour tree and in the join tree branch decomposition, the second branch of the join tree branch decomposition $(1, 9)$ does not occur as a pair in the contour tree.
The same holds for the branch decomposition of the split tree and the persistent homology pairs of the ascending and descending filtration - they all pair the global minimum $1$ with the global maximum $9$.
This cannot occur in the branch decomposition of the contour tree because branches represent monotone paths.
There is no monotone path between the $1$ and $9$ in the mesh and therefore no monotone path in the contour tree.
The result then follows.

%The set of branches produced by branch decomposition of the merge trees are not the same as the set of branches of the contour tree.

%In particular observe that the w-distance between the minimum $1$ and the maximum $9$ and there is no monotone path between them.
%As branch decomposition pairs monotone path it is not possible for the branch decomposition of the contour tree to pair the global minimum and the global maximum.
%This contradicts the results we have obtained from extended persistence.


%By extension we have shown that the set of persistence pairs are also not the same as the pairs given by the branch decomposition of the contour tree.


%This is false, except for contour tree of w-diameter 0 or 1.



% Say that in a simple domain the global min pairs with the global maximum






%The cancelation pairs in the ascending filtration areidentical to the branch decomp of the split tree and etc for join...
%As shown in figure 6
%However for the BD of the CT we are not guaranteed that BD will match either set of pairs.
%Observe that the ascending filt cancels minima with split saddles and the descending maxima with join saddles.
%it is therefore seductively plausable to take pairs from split and join to the contour tree
%This is false, except for CT of w-diameter 0 or 1 
%Refer back to fig 2 consider it, we have already demonstrated that the branch decomp of the join and split are the cancelation pairs of the correspoding asc and desc filtrations.
%These are not the same as the branches of the BD of the contour tree
%And in particular that min 1 and 9 have a w distance of 2.
%They must have an even w distance if you go down you gotta go back up!
%Therefore if as here the global min and max have w dista > 1 they cannot be on a monotone path an dcannot pair which contraditcs the extended persistence where they do
%the result then follows

%the maxima ni the centour tree cancel with join saddles as distated 
















%We are now ready to show that the branch decomposition of the contour tree does not produce equivalent results to persistent homology.
%In both the ascending and descending filtrations the global minimum $1$ pairs with the global maximum $9$.
%In the contour tree however there is no monotone path between them.
%Therefore they cannot be a pair in the branch decomposition of the contour tree.

%To go a step further we examine the join tree branch decomposition and the extended persistence of the descending filtration as well as the split tree branch decomposition and the persistence of the ascending filtration.
%Both convey essentially the same information - how the connectivity of connected components changes in the ascending or descending filtrations of the mesh.
%This why have have merged the two in a single structure in Figure \ref{fig:filtration}.
%A deeper results is that extended persistence is not equivalent to branch decomposition for the contour tree is because we cannot express it as a filtration.
%Do not say that if you cannot prove it.
%This results points out one limitation of persistent homology.
%It can only act on objects which can be expressed as a filtration.





%One potential issue with persistent homology is that not all critical points are necessarily paired. 
%Some are left out because pairs which contain essential homology classes have infinite persistence. 
%Extended persistence extends persistent homology by pairing the critical points that correspond to essential homology classes with other remaining unpaired critical points \cite{persistence-extended}.
%The main idea behind extended persistence is to follow the ascending filtration of persistent homology with a descending filtration (or vice versa). 
%In the ascending filtration we keep track of which the essential homology classes are and on the way down in the descending filtration once we reach a class that is homologous to an essential class we consider it to be destroyed and thus paired. 







%We will only be concerned with the 0th homology group.
%The homology groups consist of homology classes which correspond to structures 

%By increasing the isovalue we produce an ascending sweep and by decreasing it we produce an ascending sweep.
%Furtermore the join and split trees are an expression of how the connectivity of the sublevel and superlevel sets changes respectively.



%This means that the persistent homology of the ascending filtration describes the evolution of the connectivity of the sublevel sets and the 0th persistent homology of the descending filtration describes the evolution of the connectivity of the super level sets.
%It follows that the persistent homology of the ascending filtration expressed the same computation as the split tree.
%The same holds for the persistent homology of the descending filtration and the join tree.


%As we wi

%The 0th homology group or $H_0$ describes the connected components, the 1st homology group $H_1$ describes the cycles, the 2nd homology group $H_2$ the voids and $H_n$ their n-dimensional equivalents. 


%For example in Figure \ref{fig:example-filtration} two connected components appear, one in $M_1$ and one in $M_2$. When they merge in $M_3$ we say that one of them dies and the other one persists. Later in the filtration two cycles appear in $M_4$ and one of them dies in $M_5$ when it is filled in by a boundary. 

%The barcode diagram in the Figure \ref{fig:example-filtration} is a visual representation of this process. The horizontal lines represent timelines of the births and deaths of individual homology classes. The black dots represent that a homology class in present at that time in the filtration and the white dot denotes the death or merger of two classes.

%Persistent homology can formally be expressed as a sequence of homology groups that have functions between them induced by the subset relation of the complexes $H_\bullet(M_0) \to H_\bullet(M_1) \to ... \to H_\bullet(M_{4}) \to H_\bullet(M_9)$, where $H_\bullet$ is a placeholder for any homology group. We use the following terminology when we discuss the evolution of homology classes. A homology class is \textbf{born} if it is not in the image of the homology group of the previous complex.  A homology class \textbf{dies} if its image is the zero element or it merges with an older class.  A homology class \textbf{persists} if its image is not the zero element and it does not merge with an older class.

%We can identify a persistence pair with every homology class in the filtration. Let $\alpha$ be a homology class that is born in $M_i$ and dies in $M_j$. We call the pair $(i, j)$ the persistence pair of $\alpha$ and say that the persistence of $\alpha$ is $|j - i|$. Some classes however do not have a defined death time. These are the classes of the final complex in the filtration. We will call those classes essential and set their persistence to $\infty$. The intuition behind this is that classes that have persisted for a large number of timesteps are deemed significant while ephemeral classes are not. For example the persistence pairs in Figure \ref{fig:example-filtration} are $(2, 3)$ and $(1, \infty)$ in the 0th homology and $(4, 5)$ and $(4, \infty)$ in the 1st homology.

%In order to use persistent homology to simplify the simplicial mesh $M$ from Figure \ref{fig:filtration} we will define the ascending and descending filtration of $M$. The ascending filtration of $M$ is made of up complexes $M_i$ which contain all vertices smaller than or equal to $i$ and all other simplices between them. Analogously $M^i$ contains all vertices bigger than of equal to $i$ and all other simplices between them. 

%One potential issue with persistent homology is that not all critical points are necessarily paired. 
%Some are left out because pairs which contain essential homology classes have infinite persistence. 
%Extended persistence extends persistent homology by pairing the critical points that correspond to essential homology classes with other remaining unpaired critical points \cite{persistence-extended}.
%The main idea behind extended persistence is to follow the ascending filtration of persistent homology with a descending filtration (or vice versa). 
%In the ascending filtration we keep track of which the essential homology classes are and on the way down in the descending filtration once we reach a class that is homologous to an essential class we consider it to be destroyed and thus paired. 

%\subsection{Relation to Branch Decomposition}

%In 2004 Pascucci et. al published the original paper that introduced contour tree simplification via branch decomposition \cite{ct-branch-decomp}.
%In that paper it is clearly stated that there is a similarity between branch decomposition and persistent homology \cite{persistence-original}.
%%The authors do not describe how the persistence of branches can be derived formally within the framework of persistent homology.
%However the relation between the two is not fully explored in the paper nor in subsequent publications.
%To rectify this we answer the following question: are the pairs of critical points produced by branch decomposition the same as the pairs produced by the persistent homology?

%We can immediately demonstrate that they are not based solely on the fact that essential 0th homology classes (or connected components) have infinite persistence and all branches have finite persistence.
%This issue can be remedied by the use of extended persistence.
%In this section we will present an example which demonstrates that at least one pair of critical points is different in branch decomposition and in extended persistence.
%This example will be based on the simplicial mesh we presented in Figure \ref{fig:mesh-join-split-contour} and our familiar w-structures.
%A defining feature of this example is that the w-structure in the contour tree of the simplicial mesh prevents the existence of a monotone path between the global minimum and global maximum.
%Since monotone paths in the contour tree correspond to monotone paths in the simplicial mesh \cite{ct-big-paper} then branches obtained via branch decomposition correspond to valid topological cancellations in the simplicial mesh \cite{ct-branch-decomp}.
%Therefore the critical points obtained via branch decomposition will correspond to critical points in the simplicial mesh and as such will be directly comparable to critical points produces by the extended persistence of a filtration of the mesh.
%%Conversely the 0th persistence homology can be computed either on a filtration of the simplicial mesh or on a filtration of the contour tree directly. The output will be the same because the contour tree records the changes in connectivity of level sets of the simplicial mesh.

%%but extended persistence was introduced in a paper by Edelsbrunner et. al \cite{persistence-extended} published in 2009. Even though this general framework for pairing essential homology clases was published five years after the original paper on branch decomposition \cite{ct-branch-decomp} a previous paper \cite{extreme-elevation} by Edelsbrunner et. al published in 2004 outlines a method of pairing the essential homology classes of 2-manifolds. This method is a special case of what was later defined as extended persistece which is still applicable to branch decomposition.  Since it was published in the same year the paper by Pascucci et. al \cite{ct-branch-decomp} we will extend our comparison to the pairs of critical points obtained by extended persistence as well. The question we pose is now the following - are the pairs of critical points produced by branch decomposition the same as the pairs produced by the extended persistent of the zeroth homology?


%%For completeness we will present the ascending and descending filtration of the contour tree in Appendix \ref{chapter-asc} and Appendix \ref{chapter-desc}, but we will not use them here directly. To conclude we will compute and compare the following:

%%\begin{itemize}

    %%\item The contour tree (Figure \ref{fig:mesh-join-split-contour} b), join tree (Figure \ref{fig:mesh-join-split-contour} c) and split tree (Figure \ref{fig:mesh-join-split-contour} d).

    %%\item The branch decomposition of the contour tree (Figure \ref{fig:branch-decomp}, join tree (Figure \ref{fig:all-in-one} b) and split tree (Figure \ref{fig:all-in-one} d).

    %%\item Ascending filtration (Figure \ref{fig:asc-filtration}) and Descending filtration (Figure \ref{fig:desc-filtration}) of the simplicial mesh. In these filtrations $M_i$ is spanned by the first $i$ vertices of the mesh and $M^i$ by the last $i$ vertices of the mesh.

    %%\item The extended persistence of the 0th homology of the ascending (Figure \ref{fig:all-in-one} c) and descending (Figure \ref{fig:all-in-one} a) filtration of the simplicial mesh.

%%\end{itemize}

%We begin by computing the 0th persistent homology pairs of the ascending and descending filtration.
%Since the connected components in the filtration correspond to 0th homology classes we can compute the persistence by inspection.
%That is 0th homology classes are born when a new connected component appears and they die when two connected components merge.
%As we will only use to 0th homology classes we will refer to them simply as homology classes and use the terms connected components and homology classes interchangeably.

%Observe Figure \ref{fig:filtration} c) that in the ascending filtration there are two births and one death.
%One connected component appears in the complex $M_1$, another one in the complex $M_2$ and they merge in $M_5$.
%When the two merge we enforce the Elder Rule \cite{comp-topo} to decide which one persists and which one dies.
%The homology class born in $M_1$ is older and so it persists and the one born in $M_2$ dies.
%All complexes until the rest of the filtration consist of a single connected component.
%Threfore the homology class born in $M_1$ is essential as it persists to the end of the filtration.
%It represents the single connected component in the end of the filtration, or the simplicial mesh $M$ itself.
%The first persistence pair $(1, \infty)$ corresponds to this essential homology class and the second pair $(2, 5)$ corresponds to the inessential homology class that is born in $M_2$ dies in $M_5$.

%Before using extended persistence to pair the essential class of the ascending filtration we will compute the persistent homology of the descending filtration as well.
%In Figure~\ref{fig:filtration}~d) we can see that three homology classes are born in the complexes $M^9$, $M^8$ and $M^6$.
%The one born in $M^8$ dies in $M^4$ when it merges with the one born in $M^9$ (because $M^9$ is older).
%The one born in $M^6$ dies in $M^3$ when it merges with the one born in $M^9$.
%The one born in $M^9$ is an essential homology class because it does not die for the duration of the descending filtration.
%Threfore the persistence pairs are $(6, 3)$, $(8, 4)$ and $(9, \infty)$.


%Finally we must pair the essential homology classes from both filtrations using extended persistence.
%In the ascending filtration the first vertex we meet that represent an essential homology class is the global minimum born in $M_1$.
%In the descending filtration the first vertex we meet that represent an essential homology class is the global maximum born in $M^9$.
%Both of these essential homology classes represent the same class, that is the simplicial mesh $M$ itself.
%In other words $M_1$ is first complex in the ascending filtration that contains the essential class and as we have just seen $M^9$ is the first complex in the descending filtration that contains the same class.
%And vice versa.
%Thus we pair $(1, 9)$ in the ascending filtration and $(9, 1)$ in the descending filtration.

%We are not ready to show that the branch decomposition of the contour tree does not produce equvialent results to persistent homology.
%In both the ascending and descending filtrations the global minimum $1$ pairs with the global maximum $9$.
%In the contour tree however there is no monotone path between them.
%Therefore they cannot be a pair in the branch decomposition of the contour tree.

%To go a step further we examine the join tree branch decomposition and the extended persistence of the descending filtration as well as the split tree branch decomposition and the persistence of the ascending filtration.
%%We have depicted each of these side by side on Figure \ref{fig:filtration}.
%%The only change we have made is to relabel the time component of the descending filtration to correspond to the vertex index that appears at a given time.
%Both convey essentially the same information - how the connectivity of connected components changes in the ascending or descending filtrations of the mesh.
%This why have have merged the two in a single structure in Figure \ref{fig:filtration}.
%A deeper results is that extended persistence is not equivalent to branch decomposition for the contour tree is because we cannot express it as a filtration.
%This results points out one limitation of persistent homology.
%It can only act on objects which can be expressed as a filtration.
