\section{Background}

%The preliminaries for them include Point Set Topology [Munkres], Differential Topology [Morse] and Algebraic Topology [Hatcher].  

%The two key concepts we will introduce as part of the necessary background are the Contour Tree and Persistent Homology. We will opt for a more practical and computationally flavoured introduction. 

In this section, we describe the Contour Tree (\ref{sec:ContourTrees}), and methods to compute it (\ref{sec:ctalgorithms}) and to simplify it (\ref{sec:ctsimplification}). For clarity, we leave persistent homology to \secref{phbd}. 

\subsection{Contour Trees}
\label{sec:ContourTrees}

\begin{figure*}[h]%
    \centering

    \subfloat[Simplicial mesh.]{{\includegraphics[scale=0.04]{./images/filtration/asc/x9.pdf}}}%
    \qquad
    \qquad
    \subfloat[Contour tree.]{{\includegraphics[scale=0.08]{./images/w3x3/w3x3-contour-tree.pdf}}}%
    \qquad
    \qquad
    \subfloat[Join tree.]{{\includegraphics[scale=0.08]{./images/w3x3/w3x3-join-tree.pdf}}}%
    \qquad
    \qquad
    \subfloat[Split tree.]{{\includegraphics[scale=0.08]{./images/w3x3/w3x3-split-tree.pdf}}}%

    \caption{A simplicial mesh that generates a W-structure, the corresponding contour tree with the W-structure in thicker edges and the corresponding merge trees. We label the vertices with their height value.}%

    \label{fig:mesh-join-split-contour}%
\end{figure*}


We assume that we have a function $f: M \to \mathbb{R}$ on a manifold $M$.  A \emph{level set} is the set of all points with a given \emph{isovalue} $h$: $f^{-1}(\{h\}) = \{x \in M~|~f(x) = h\}$.  This level set may have multiple connected components, in which case we will refer to individual connected components as \emph{contours}.  As $h$ varies, contours
may appear, disappear, connect or disconnect at \emph{critical points} where the gradient vector is zero.  

We define the Reeb Graph for $f$ by contracting each contour at each isovalue to a single point: the result skeletonizes $f$ to a graph whose vertices are critical points and whose edges represent families of contours with identical topology. In the case of a simple domain, the graph is connected and acyclic, and the Reeb Graph is known as the Contour Tree~\cite{BR63}.

We can also relax the definition of the level set to the super-level set: the set of points with higher values than $h$, i.e. $\{x \in M~|~f(x) \geq h\}$, and the sub-level set with lower values, $\{x \in M~|~f(x) \leq h\}$.  Contracting the connected components of these gives two \emph{merge trees} which are connected and acyclic, and are sometimes referred
to as the join \& split trees.

\begin{figure*}[h]%
    \centering
    \subfloat[Contour Tree]{{\includegraphics[scale=0.08]{./images/w3x3/w3x3-contour-tree-decomposition-vertical.pdf}}}%
    \qquad
    \qquad
    \qquad
    \subfloat[Join Tree]{{\includegraphics[scale=0.08]{./images/w3x3/w3x3-join-tree-decomposition-vertical.pdf}}}%
    \qquad
    \qquad
    \qquad
    \subfloat[Split Tree]{{\includegraphics[scale=0.08]{./images/w3x3/w3x3-split-tree-decomposition-vertical.pdf}}}%
    \caption{Branch decomposition of the contour tree and the two merge trees from \figref{mesh-join-split-contour} with the edges of the master branches of given in thicker lines. In both merge trees the master branch is the monotone path from the global minimum $1$ to the global maximum $9$. Note that in the absence of a monotone path between $1$ and $9$ in the mesh and its contour tree they cannot be paired. See text for details.}%
    \label{fig:branch-decomp}%
\end{figure*}

\subsection{Contour Tree Algorithms}
\label{sec:ctalgorithms}

In practice, we assume that $M$ is approximated by a mesh with interpolant, commonly a simplicial mesh with linear interpolation, although this assumption can be relaxed~\cite{CS09}.  Under these constraints, critical points occur at vertices, and only the graph composed of the vertices and edges of the mesh needs to be processed.

The standard algorithm~\cite{ct-big-paper} is based on the idea of an isovalued sweep - i.e. processing the vertices of the mesh in sorted order from high to low.  As each vertex $u$ is processed, any edge $(u,v)$ to a higher-valued vertex $v$ is also processed.  At each step, therefore, there is a subgraph representing the super-level set, whose connectivity can be tracked with an incremental version of the union-find data structure~\cite{Tar75}.

The algorithm exploits this to construct the join tree, then repeats with a low-to-high sweep to compute the split tree. Once this has been done, the two merge trees are combined in a final phase that transfers leaves one at a time, using induction on a simple invariant to guarantee correctness.  As a result, this algorithm is sometimes referred to as the \emph{sweep and merge} algorithm.

More recently, algorithms have been described for distributed \cite{distributed-ct-algo, distributed-ct-algo-2} and shared memory parallelism paradigms \cite{parallel-peak-pruning, parallel-ct-1, task-merge-tree}.  The focus of this paper will be a recent shared memory algorithm for contour tree computation \cite{parallel-peak-pruning}, which parallelises the existing sweep and merge phases of the standard algorithm. We omit the details of parallelising merge tree computation because it is not directly related to the W-structure.

In the parallel merge phase, instead of transferring edges serially from the merge trees to the contour tree, a batch of all available leaves is transferred at once, and the data structures are updated accordingly.  In the ideal case, each batch transfers at least half of the vertices, guaranteeing logarithmic performance. In practice, however, this is only possible if degree 2 vertices are removed in a post process for each batch: if this is done, then the ideal case is achieved.

Removing a degree 2 vertex in parallel is straightforward when the neighbouring vertices have values spanning the vertex $v$, especially if $v$ is connected by a chain of such vertices to a leaf.  In effect, these vertices are regular at this stage (although they may not have been in earlier stages), and can be removed from the tree.  

For vertices of degree $2$ (forks), this is not so easy to perform, and a \emph{W-structure} consists of repeated forks zigzagging between upwards and downwards, as illustrated in Figure~\ref{fig:mesh-join-split-contour}.
Notice that in order to collapse the W-structure completely we can only prune from an endpoint of the W-structure to a fork. The internal vertices cannot be process until we have pruned all forks. Therefore computation is effectively serialized along the largest W-structure in the contour tree.

\subsection{Contour Tree Simplification}
\label{sec:ctsimplification}

Contour trees of data may be very large: their utility depends on simplifying them by removing parts of them that correspond to less significant topological features or sampling noise and error.

The principal technique for contour tree simplification is branch decomposition \cite{ct-branch-decomp}. The contour tree is decomposed into a set of disjoint monotone paths (branches) which cover all edges of the tree. In a monotone path all vertices along the path have a strictly ascending or descending height value. The trivial branch decomposition assigns every edge to a separate branch. A branch decomposition is hierarchical when there is exactly one branch that connects two leaves called the master branch and every other branch connects a leaf to an interior node. An example of a hierarchical branch decomposition is shown in Figure \ref{fig:branch-decomp}.

The branches in this scheme represent pairs of critical points and form a basis for topological simplification. 
We define the persistence of a branch to be the greater of the difference between the height value at its endpoints and the persistence of its children. 
Branches of high persistence reflect more prominent features in the tree. 
The topological simplification consists of removing branches that do not disconnect the tree in order of their persistence. This produces a hierarchy of cancellations as shown in Figure \ref{fig:branch-decomp}. 
We apply the simplification by repeatedly removing the branches with lowest persistence that do not disconnect the tree.

As an example we will compute the hierarchical branch decomposition of the contour tree from Figure \ref{fig:mesh-join-split-contour}. 
Note that the vertices are uniquely labeled with their height.
The first two candidate branches we identify are $(6, 3)$ with persistence $3$ and $(4, 9)$ with persistence $5$. 
We take the branch with lower persistence $(6, 3)$. 
In the next step the candidate branches are $(1, 5)$ with persistence $4$ and $(4, 9)$ with persistence $5$. We take $(1, 5)$.
Afterwards the remaining candidate branches are $(4, 8)$ with persistence $4$ and $(4, 9)$ with persistence $5$. 
After removing $(4, 8)$ in the final stage the only remaining branch is $(2, 9)$. It is the master branch because it is the last one left and it connects two leaves.
To conclude, the pairs of critical points produced by the branch decomposition of the contour tree are $(3, 6), (1, 5), (4, 8)$ and $(2, 9)$.

We can also compute the branch decomposition of the join and split trees. The candidate branches for the join tree are $(3, 6)$, $(4, 8)$ and $(4, 9)$. We remove them in order of persistence. First $(3, 6)$, then $(4, 8)$ and finally the master branch $(1, 9)$. In the split tree the two candidate branches are $(1, 5)$ and $(2, 5)$. We remove $(2, 5)$ first because it has lower persistence and then the master branch $(1, 9)$.

